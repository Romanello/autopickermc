package xyz.romanello.pluginMc; 
import org.bukkit.plugin.java.JavaPlugin; 


import java.util.*;
import java.util.Calendar;
import java.text.DateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.text.SimpleDateFormat;
import org.bukkit.*;
 

public class App extends JavaPlugin {
 
    @Override
    public void onEnable() { 
        //getServer().getPluginManager().registerEvents(new EventListenerBlock(this), this);
        //getServer().getPluginManager().registerEvents(new EventListenerItem(this), this);
        getServer().getPluginManager().registerEvents(new EventListenerInventory(), this);
        getServer().getPluginManager().registerEvents(new EventListenerPlayer(), this);
        //orario();
        getLogger().info("Autoclicker caricato.");
    }

    @Override
    public void onDisable() {
        getLogger().info("Autoclicker chiuso.");
    }

    public void orario()
    {
        int milisInAMinute = 60000; 
        LocalDateTime start = LocalDateTime.now(); 
        LocalDateTime end = start.plusMinutes(1).truncatedTo(ChronoUnit.MINUTES);
        Duration duration = Duration.between(start, end);
        long millisToNextMinute = duration.toMillis(); 
        
        Timer timer = new Timer(); 
        timer.schedule(new TimerTask() {
            public void run() {
                Date now = Calendar.getInstance().getTime();
                SimpleDateFormat ora = new SimpleDateFormat("hh"); 
                SimpleDateFormat minuto = new SimpleDateFormat("mm"); 
                //Server è indietro di 2 ore
                if((ora.format(now).toString().equals("02") || ora.format(now).toString().equals("14")) && minuto.format(now).toString().equals("20"))
                {
                    Bukkit.broadcastMessage(ChatColor.GREEN.toString() + "Sono le 4:20"); 
                    getLogger().info("[PLUGIN] Sono le 4:20");
                }
            }
        }, millisToNextMinute, milisInAMinute);
    
    }   
}

