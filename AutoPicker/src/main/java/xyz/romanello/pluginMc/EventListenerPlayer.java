package xyz.romanello.pluginMc; 
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.inventory.*;
import org.bukkit.event.*;
import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.Material; 
import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.event.player.PlayerBedEnterEvent.BedEnterResult;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.block.*;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.plugin.*; 
import org.bukkit.World.*;

import java.util.*;

public class EventListenerPlayer implements Listener
{
    /*
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        event.setJoinMessage(ChatColor.YELLOW.toString() + "Il cuck di " + event.getPlayer().getName() + " e' entrato.");
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event)
    {
        event.setQuitMessage(ChatColor.YELLOW.toString() + "Il cuck di " + event.getPlayer().getName() + " e' uscito.");
    }
    */

    @EventHandler 
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        Bukkit.broadcastMessage(ChatColor.GREEN.toString() + "F F F F "); 
        Player player = event.getEntity();
        Bukkit.broadcastMessage(ChatColor.GREEN.toString() + player.getDisplayName() + " e' morto con " + player.getLevel() + " livelli :)");
        Bukkit.getLogger().info(ChatColor.GREEN.toString() + player.getDisplayName() + " e' morto a X:" + player.getLocation().getX() + " Y:" + player.getLocation().getY() + " Z:" + player.getLocation().getZ());
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) 
    {
        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();
        if(event.getMessage().equals("vitaoggetto") && item.getType().getMaxDurability() != 0)
        {
            player.sendMessage(ChatColor.GREEN.toString() + ChatColor.ITALIC + (item.getDurability()) + " usato su " + item.getType().getMaxDurability()); 
        } 

        if(event.getMessage().equals("stats"))
        {
            //Stampo le stats di tutti i giocatori
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) 
            {
                PlayerStats stats = new PlayerStats(onlinePlayer); 
                player.sendMessage(ChatColor.GREEN.toString() + onlinePlayer.getDisplayName() + ChatColor.WHITE.toString() + " ha giocato " + stats.getTotalTimePlayed() + ", ha fatto " + stats.damageDone() +" danni, ucciso " + stats.entityKills() + " entita', e' morto " + stats.deathsNumber() + " volte, rotto " + stats.blocksMined() +" blocchi e aperto "+ stats.getChestOpened() +" ceste.");
                //player.sendMessage(ChatColor.GREEN.toString() + player.getDisplayName() + ChatColor.WHITE.toString() +" ha fatto " + stats.damageDone() +" danni, ucciso " + stats.entityKills() + " entita', e' morto " + stats.deathsNumber() + " volte, rotto " + stats.blocksMined() +" blocchi e aperto "+ stats.getChestOpened() +" ceste.");
            }
        } 

        if(event.getMessage().equals("vitaoggetto") || event.getMessage().equals("stats")) {
            event.setCancelled(true);
        }

    } 

    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent event)
    {  
        if(event.getPlayer().getWorld().getEnvironment() == Environment.NORMAL && BedEnterResult.OK == event.getBedEnterResult​()) {
            Bukkit.broadcastMessage(ChatColor.GREEN.toString() + event.getPlayer().getDisplayName() + ChatColor.WHITE.toString() + " sta dormendo");
        }
    }
    

    @EventHandler
    public void onBedLeave(PlayerBedLeaveEvent event)
    { 
        if(event.getPlayer().getWorld().getEnvironment() == Environment.NORMAL) {
            Bukkit.broadcastMessage(ChatColor.GREEN.toString() + event.getPlayer().getDisplayName() + ChatColor.WHITE.toString() + " si e' alzato");
        }
    }
}