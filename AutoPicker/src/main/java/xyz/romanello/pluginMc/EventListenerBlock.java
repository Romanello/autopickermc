package xyz.romanello.pluginMc; 
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.inventory.*;
import org.bukkit.event.*;
import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.Material; 
import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.block.*;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.plugin.*;

import java.util.*;

public class EventListenerBlock implements Listener
{
    Plugin plugin;

    public EventListenerBlock(Plugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        final Player player = event.getPlayer(); 
        Block bloccoPiazzato = event.getBlockPlaced();
        ItemStack item = event.getItemInHand(); 
        
        //item.getAmount() se e' 1 vuol dire che ho piazzato l'ultimo blocco
        if (item.getAmount() == 1) {
            //Non ha in mano nulla, trovo un oggetto sull'inventario come quello precedente che aveva in mano
            
            if(player.getInventory().contains(bloccoPiazzato.getType()))
            { 
                //Ottengo le posizioni di tutti gli oggetti dello stesso tipo che aveva in mano
                HashMap<Integer, ? extends ItemStack> posizioneItem = player.getInventory().all(bloccoPiazzato.getType());
                 
                Set<Integer> posizioni = posizioneItem.keySet();
                Integer posElementoPresenteInventario = -1;
                Integer stackRimanenti = 0;
                for(Integer pos: posizioni){
                    //Evito di prendere la posizione di quello che avevo in mano e che ho finito
                    if(pos != event.getPlayer().getInventory().getHeldItemSlot())
                    {
                        posElementoPresenteInventario = pos;
                    }
                    stackRimanenti+=event.getPlayer().getInventory().getItem(pos).getAmount();
                }
                
                if(posElementoPresenteInventario != -1 && player.getInventory().getItemInMainHand().getType().equals(bloccoPiazzato.getType()))
                {
                    final ItemStack elementoInventario = player.getInventory().getItem(posElementoPresenteInventario);
    
                    //final int posMano = player.getInventory().getHeldItemSlot();
                      
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public void run() {
                            //player.getInventory().setItem(posMano, elementoInventario);
                            player.getInventory().setItemInMainHand(elementoInventario);
                        }
                    }, 5L);

                    player.getInventory().clear(posElementoPresenteInventario);
    
                    player.updateInventory();

                    player.sendMessage(ChatColor.BLUE.toString() + "Ti rimangono " + (stackRimanenti-1) + " item di " + bloccoPiazzato.getType().toString());
                } 
            } 
        } 
    }

    /*@EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        player.sendMessage("Blocco rotto");
    }*/

}
