package xyz.romanello.pluginMc; 
import org.bukkit.plugin.java.JavaPlugin; 
import org.bukkit.entity.*;
import org.bukkit.Statistic;
import org.bukkit.*;
import org.bukkit.Material; 

public class PlayerStats 
{
    Player player;
    public PlayerStats(Player player)
    {
        this.player = player;
    }

    public int getChestOpened()
    {
        return player.getStatistic(Statistic.CHEST_OPENED);
    }
    public int deathsNumber()
    {
        return player.getStatistic(Statistic.DEATHS);
    }
    public int entityKills()
    {
        return (int)player.getStatistic(Statistic.MOB_KILLS) + (int)player.getStatistic(Statistic.PLAYER_KILLS);
    } 
    public int blocksMined()
    {
        int blocchiRotti = 0;
        for(Material material : Material.values() ) {
            blocchiRotti += player.getStatistic(Statistic.MINE_BLOCK, material);
        }
        return blocchiRotti;
    }
    public int damageDone()
    {
        return player.getStatistic(Statistic.DAMAGE_DEALT); 
    }
    public String getTotalTimePlayed() { 

        long totalTicks = player.getStatistic(Statistic.PLAY_ONE_MINUTE);

        long days = (totalTicks / 1728000);
        totalTicks -= days*1728000;
        long hours = (totalTicks / 72000); 
        totalTicks -= hours*72000;
        long minutes = (totalTicks / 1200);
        totalTicks -= minutes*1200;
        long seconds = (totalTicks / 20);


        if (days > 0) 
        { 
            return days + " d, " + hours + " o, " + minutes + " m, " + seconds + " s";
        } 
        else 
        {
            if (hours > 0) {
                return hours + "o, " + minutes + "m, " + seconds + "s";
            } 
            else 
            {
                if (minutes > 0) {
                    return minutes + "m, " + seconds + "s";
                } 
                else 
                {
                    return seconds + "s";
                }
            }
        }
    }
}