package xyz.romanello.pluginMc; 
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.inventory.*;
import org.bukkit.event.*;
import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.Material; 
import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.block.*;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.plugin.*;

import java.util.*;

public class EventListenerInventory implements Listener
{
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    { 
        //if(event.getClick().name().equals("RIGHT") && event.getCurrentItem().getType().equals(Material.CRAFTING_TABLE))
        if(event.isRightClick() && event.isShiftClick() && event.getCurrentItem().getType().equals(Material.CRAFTING_TABLE))
        { 
            event.getWhoClicked().openWorkbench(null, true); 
           
            //Cancello l'evento di tasto destro altrimenti me lo prende in mano
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onSwapHandKeyPressed(PlayerSwapHandItemsEvent event)
    {
        //getOffHandItem perchè l'evento prima esegue poi controlla
        if(event.getOffHandItem().getType().equals(Material.CRAFTING_TABLE))
        {
            event.getPlayer().openWorkbench(null, true);

            //Non mettere la crafting nella mano secondaria
            event.setCancelled(true);
        } 
    }

    //Quando cambio inventario con la rotellina
    /*@EventHandler
    public void PlayerItemHeldEvent(PlayerItemHeldEvent event) {
         
        Bukkit.getLogger().info("Triggered, from " + event.getPreviousSlot() + " to " + event.getNewSlot());
        
    }*/
}