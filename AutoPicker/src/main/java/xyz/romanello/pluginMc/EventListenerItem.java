package xyz.romanello.pluginMc; 
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.inventory.*;
import org.bukkit.event.*;
import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.Material; 
import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.block.*;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.plugin.*;

import java.util.*;

public class EventListenerItem implements Listener
{
    Plugin plugin;

    public EventListenerItem(Plugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onToolDamange(PlayerItemDamageEvent event) {
        final Player player = event.getPlayer();
        int dannoRiceve = event.getDamage();
        ItemStack item = event.getItem();
    
        if((item.getType().getMaxDurability() <= (item.getDurability()) + dannoRiceve) )
        {
            //player.sendMessage(item.getType().toString() + " rotto"); 

            if(player.getInventory().contains(item.getType()))
            { 
                //Ottengo le posizioni di tutti gli oggetti dello stesso tipo che aveva in mano
                HashMap<Integer, ? extends ItemStack> posizioneItem = player.getInventory().all(item.getType());
                 
                Set<Integer> posizioni = posizioneItem.keySet();
                Integer posElementoPresenteInventario = -1;
                Integer stackRimanenti = 0;
                for(Integer pos: posizioni){
                    if(pos != player.getInventory().getHeldItemSlot())
                    {
                        posElementoPresenteInventario = pos;
                    }
                    stackRimanenti+=1;
                } 
                
                if(posElementoPresenteInventario != -1 && player.getInventory().getItemInMainHand().getType().equals(event.getItem().getType()))
                {
                    
                    final ItemStack elementoInventario = player.getInventory().getItem(posElementoPresenteInventario); 
                    //final int posMano = player.getInventory().getHeldItemSlot();
                      
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public void run() {
                            //player.getInventory().setItem(posMano, elementoInventario);
                            player.getInventory().setItemInMainHand(elementoInventario);
                        }
                    }, 5L);
                    
                    player.getInventory().clear(posElementoPresenteInventario);
               
                    player.updateInventory(); 

                    player.sendMessage(ChatColor.BLUE.toString() + "Ti rimangono " + (stackRimanenti-1) + " item di " + item.getType().toString());
                } 
            } 
        } 
    } 

    //Per pozioni, patate ecc
    /*@EventHandler 
    public void onItemConsume(PlayerItemConsumeEvent event) {
        
        Player player = event.getPlayer(); 
        ItemStack item = event.getItem();
        Bukkit.getLogger().info("Usato " +  item.getItemMeta().getDisplayName().toString());
        player.sendMessage("Hai usato: " + item.getItemMeta().getDisplayName() + " qta:" + item.getAmount() + " type: "+item.getType().toString());
        
    }*/

}